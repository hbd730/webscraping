import urllib2
from bs4 import BeautifulSoup
import requests

form_url = 'https://subsidytracker.goodjobsfirst.org/form.php'
page = urllib2.urlopen(form_url)
soup = BeautifulSoup(page, 'html.parser')

options = [str(x['value']) for x in soup.find(id="edit-field-subsidy-parent-value").find_all('option')]
# first one is empty
options.pop(0)

for option in options:
    download_url = "https://subsidytracker.goodjobsfirst.org/prog.php?parent=%s&detail=export_csv" % option
    print download_url
    r = requests.get(download_url, allow_redirects=True)
    filename = option + '.csv'
    open(filename, 'wb').write(r.content)

